//
//  LocationAdmin.swift
//  Actividad1
//
//  Created by Mixtomeister on 20/4/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit
import CoreLocation

class LocationAdmin: NSObject, CLLocationManagerDelegate {
    
    static let admin:LocationAdmin = LocationAdmin()
    
    var locationManager: CLLocationManager?
    var delegate: LocationAdminDelegate?
    
    override init(){
        super.init()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate?.actualizarLocation(coordenada: locations[0].coordinate)
        locationManager?.stopUpdatingLocation()
    }
    
    func actualizarLocation(){
        locationManager?.startUpdatingLocation()
    }
}

protocol LocationAdminDelegate{
    func actualizarLocation(coordenada: CLLocationCoordinate2D)
}
