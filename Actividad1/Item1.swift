//
//  Item1.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 15/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class Item1: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var hola: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (DataHolder.data.circuitos?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Protocelda:item1_celda = tableView.dequeueReusableCell(withIdentifier: "ProtoCelda", for: indexPath) as! item1_celda
        AdminFirebase.instancia.observarCircuito(cir: indexPath.row, celda: Protocelda)
        return Protocelda
    }

}
