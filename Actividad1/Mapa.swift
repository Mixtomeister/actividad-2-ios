//
//  Mapa.swift
//  Actividad1
//
//  Created by Mixtomeister on 20/4/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit
import MapKit

class Mapa: UIViewController, LocationAdminDelegate {

    @IBOutlet var mapa:MKMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationAdmin.admin.delegate = self
        LocationAdmin.admin.actualizarLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func actualizarLocation(coordenada: CLLocationCoordinate2D) {
        let region: MKCoordinateRegion = MKCoordinateRegionMake(coordenada, MKCoordinateSpanMake(0.05, 0.05))
        mapa?.setRegion(region, animated: true)
    }
}
