//
//  Login.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 14/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class Login: UIViewController, AdminFirebaseDelegate {

    @IBOutlet var txt_user: UITextField?
    @IBOutlet var txt_pass: UITextField?
    @IBOutlet var lbl_login: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func entrar() {
        if(txt_user?.text == "" || txt_pass?.text == ""){
            mostrarError(msg: "Email o contraseña vacio")
        }else{
            AdminFirebase.instancia.iniciarSesion(email: (txt_user?.text)!, pass: (txt_pass?.text)!, delegate: self)
        }
    }
    
    func mostrarError(msg: String) {
        lbl_login?.text = msg
    }
    
    @IBAction func ocultarLabel(){
        lbl_login?.text = ""
    }
    
    func inicioSesion(err: Int) {
        if (err == 0){
            AdminFirebase.instancia.datosCircuitos(delegate: self)
        }else{
            txt_pass?.text = ""
            mostrarError(msg: "Email o contraseña incorrecto")
        }
    }
    
    func cargadosDatos() {
        self.performSegue(withIdentifier: "loginTrans", sender: self)
    }
}
