//
//  DataHolder.swift
//  Actividad1
//
//  Created by Mixtomeister on 20/4/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class DataHolder: NSObject {
    static let data: DataHolder = DataHolder()
    var circuitos: Array<AnyObject>?
    var pilotos: Array<AnyObject>?
    var imgCircuitos:[String:UIImage] = [:]
    var imgPilotos:[String:UIImage] = [:]
}
