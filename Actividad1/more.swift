//
//  more.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 21/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class more: UIViewController, AdminFirebaseDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func singOut() {
        AdminFirebase.instancia.singOut(delegate: self)
    }
    
    func cierreSesion() {
        self.performSegue(withIdentifier: "backToMain", sender: self)
    }
    
    @IBAction func borrarUsuario(){
        let alertController = UIAlertController(title: "Eliminar cuenta", message: "¿Seguro que quieres eliminar tu cuenta? No se podrá recuperar", preferredStyle: UIAlertControllerStyle.alert)
        let DestructiveAction = UIAlertAction(title: "Eliminar Cuenta", style: UIAlertActionStyle.destructive) {
            (result : UIAlertAction) -> Void in
            AdminFirebase.instancia.borrarUsuario(delegate: self)
        }
        let okAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default)
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
