//
//  DataHolder.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 21/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class AdminFirebase: NSObject{
    static let instancia: AdminFirebase = AdminFirebase()
    var databaseRef: FIRDatabaseReference!
    var storageRef: FIRStorageReference!

 
    func configFirebase(){
        FIRApp.configure()
        databaseRef = FIRDatabase.database().reference()
        storageRef = FIRStorage.storage().reference()
    }
    
    func crearUsuario(email : String, pass : String, delegate: AdminFirebaseDelegate){
        FIRAuth.auth()?.createUser(withEmail: email, password: pass) { (user, error) in
            if error != nil {
                delegate.creacionUsuario!(err: -1, desc: (error?.localizedDescription)!)
            }else{
                delegate.creacionUsuario!(err: 0, desc: "")
            }
        }
    }
    
    func iniciarSesion(email : String, pass : String, delegate: AdminFirebaseDelegate){
        FIRAuth.auth()?.signIn(withEmail: email, password: pass) { (user, error) in
            if error != nil {
                delegate.inicioSesion!(err: -1)
            }else{
                delegate.inicioSesion!(err: 0)
            }
        }
    }
    
    func singOut(delegate: AdminFirebaseDelegate){
        try! FIRAuth.auth()!.signOut()
        delegate.cierreSesion!()
    }
    
    func borrarUsuario(delegate: AdminFirebaseDelegate){
        FIRAuth.auth()?.currentUser?.delete()
        delegate.cierreSesion!()
    }
    
    func datosCircuitos(delegate: AdminFirebaseDelegate){
        databaseRef.child("circuito").observeSingleEvent(of: .value, with: { (snapshot) in
            DataHolder.data.circuitos = snapshot.value as? Array<AnyObject>
            self.cargadosCircuitos(delegate: delegate)
        })
    }
    
    func observarCircuito(cir: Int, celda: item1_celda){
        let ruta = String(format: "circuito/%d", cir)
        databaseRef.child(ruta).observe(FIRDataEventType.value, with: { (snapshot) in
            let dic = snapshot.value as? NSDictionary
            celda.lbl?.text = dic?["nombre"] as? String
            celda.gp?.text = String(format: "Gran Premio de %@", (dic?["localizacion"] as? String)!)
            self.imgCircuito(ruta: (dic?["foto"] as? String)!, celda: celda)
        })
    }
    
    func imgCircuito(ruta: String, celda: item1_celda){
        celda.img?.image = nil
        if(DataHolder.data.imgCircuitos[ruta] == nil){
            storageRef.child(ruta).data(withMaxSize: 1 * 1024 * 1024) { (data, error) -> Void in
                if (error != nil) {
                    celda.img?.image = nil
                } else {
                    celda.img?.image = UIImage(data: data!)
                    DataHolder.data.imgCircuitos[ruta] = UIImage(data: data!)
                }
            }
        }else{
            celda.img?.image = DataHolder.data.imgCircuitos[ruta]
        }
        
    }
    
    func observarPiloto(pil: Int, celda: item2_celda){
        let ruta = String(format: "pilotos/%d", pil)
        databaseRef.child(ruta).observe(FIRDataEventType.value, with: { (snapshot) in
            let dic = snapshot.value as? NSDictionary
            celda.lbl?.text = String(format: "%d. %@", (dic?["numero"] as? Int)!, (dic?["nombre"] as? String)!)
            self.imgPiloto(ruta: (dic?["foto"] as? String)!, celda: celda)
        })
    }
    
    func imgPiloto(ruta: String, celda: item2_celda){
        celda.img?.image = nil
        if(DataHolder.data.imgPilotos[ruta] == nil){
            storageRef.child(ruta).data(withMaxSize: 1 * 1024 * 1024) { (data, error) -> Void in
                if (error != nil) {
                    celda.img?.image = nil
                } else {
                    celda.img?.image = UIImage(data: data!)
                    DataHolder.data.imgPilotos[ruta] = UIImage(data: data!)
                }
            }
        }else{
            celda.img?.image = DataHolder.data.imgPilotos[ruta]
        }
    }
    
    func cargadosCircuitos(delegate: AdminFirebaseDelegate) {
        databaseRef.child("pilotos").observeSingleEvent(of: .value, with: { (snapshot) in
            DataHolder.data.pilotos = snapshot.value as? Array<AnyObject>
            delegate.cargadosDatos!()
        })
    }
}

@objc protocol AdminFirebaseDelegate{
    @objc optional func creacionUsuario(err:Int, desc: String)
    @objc optional func inicioSesion(err:Int)
    @objc optional func cierreSesion()
    @objc optional func cargadosDatos()
}
