//
//  Registrar.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 14/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class Registrar: UIViewController,AdminFirebaseDelegate {
    
    @IBOutlet var txt_mail1: UITextField?
    @IBOutlet var txt_mail2: UITextField?
    @IBOutlet var txt_pass1: UITextField?
    @IBOutlet var txt_pass2: UITextField?
    @IBOutlet var lbl_error: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func volverLogin() {
        self.performSegue(withIdentifier: "registroTrans", sender: self)
    }
    
    @IBAction func registrar() {
        if(txt_mail1?.text == "" || txt_mail2?.text == "" || txt_pass1?.text == "" || txt_pass2?.text == ""){
            mostrarError(msg: "No estan todos los campos rellenos")
        }else{
            if(txt_pass1?.text == txt_pass2?.text && txt_mail1?.text == txt_mail2?.text){
                AdminFirebase.instancia.crearUsuario(email: (txt_mail1?.text)!, pass: (txt_pass1?.text)!,delegate: self)
            }else{
                mostrarError(msg: "Las contraseñas o los emails no coinciden")
            }
        }
    }
    
    @IBAction func ocultarLabel() {
        lbl_error?.text = ""
    }
    
    func mostrarError(msg: String) {
        lbl_error?.text = msg
    }
    
    func creacionUsuario(err: Int, desc: String) {
        if (err == 0) {
            let alertController = UIAlertController(title: "Registrar", message: "El usuario se creo correctamente.", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
                self.performSegue(withIdentifier: "registroTrans", sender: self)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            mostrarError(msg: desc)
        }
    }
}
