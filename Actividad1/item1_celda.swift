//
//  item1_celda.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 15/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class item1_celda: UITableViewCell {
    
    @IBOutlet var img:UIImageView?
    @IBOutlet var lbl:UILabel?
    @IBOutlet var gp:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
