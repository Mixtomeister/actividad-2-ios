//
//  item2.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 15/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class item2: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (DataHolder.data.pilotos?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item: item2_celda = collectionView.dequeueReusableCell(withReuseIdentifier: "ProtoCelda", for: indexPath) as! item2_celda
        AdminFirebase.instancia.observarPiloto(pil: indexPath.row, celda: item)
        return item
    }

}
